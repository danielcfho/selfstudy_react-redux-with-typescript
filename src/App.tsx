import React from "react";
import logo from "./logo.svg";
import { useDispatch } from "react-redux";
import { Box, Button, Center, Heading, VStack } from "@chakra-ui/react";
import { bindActionCreators } from "redux";
import { actionCreators, State } from "./state";
import { useSelector } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const { withdrawMoney, depositMoney, bankrupt } = bindActionCreators(
    actionCreators,
    dispatch
  );
  const amount = useSelector((state:State)=>state.bank)
  return (
    <Box h={"100v"}>
      <Center>
        <VStack spacing={4} align="stretch">
          <Heading textAlign={"center"}>{ amount }</Heading>
          <Button onClick={()=>depositMoney(100)}>deposit</Button>
          <Button onClick={()=>withdrawMoney(100)}>withdraw</Button>
          <Button onClick={()=>bankrupt()}>bankrupt</Button>
        </VStack>
      </Center>
    </Box>
  );
}

export default App;
